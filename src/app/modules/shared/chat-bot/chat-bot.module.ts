import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatBotComponent } from './chat-bot.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ChatBotComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports:[ChatBotComponent]
})
export class ChatBotModule { }
