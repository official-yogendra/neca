import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chat-bot',
  templateUrl: './chat-bot.component.html',
  styleUrls: ['./chat-bot.component.scss'],
})
export class ChatBotComponent implements OnInit {
  chatbotVisibility = false;
  chatMessage="";
  constructor() {}

  chatHistory=[
    {
      chat_message:"Welcome to NECA !",
      chat_user_name:"Bot",
      chat_date_time: new Date(),
      chat_css_class:"--customer"
    }

  ]

  ngOnInit(): void {}

  setChatboxVisibility(visibleStatus: boolean) {
    if (visibleStatus) {
      this.chatbotVisibility = true;
      return;
    }
    this.chatbotVisibility = false;
  }

  addChatResponse()
  {

    if(this.chatMessage)
    {
      this.chatHistory.push(
        {
          chat_message:this.chatMessage,
          chat_user_name:"Me",
          chat_date_time: new  Date(),
          chat_css_class:"--expert"
        }
      );
      this.chatMessage='';
    }
   
  }

  
}
