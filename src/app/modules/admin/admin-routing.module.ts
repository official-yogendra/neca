import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path:'',
    component:AdminComponent,
    children:[
      {
        path:'',
        redirectTo:'dashboard'
      },
      {
        path:'dashboard',    
        loadChildren:()=>import('./pages/dashboard/dashboard.module').then(m=>m.DashboardModule) 
      },
      {
        path:'manage-content',    
        loadChildren:()=>import('./pages/manage-content/manage-content.module').then(m=>m.ManageContentModule) 
      }          
    ]
  },
  
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
