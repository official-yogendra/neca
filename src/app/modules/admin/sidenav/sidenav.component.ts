import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  constructor(private route:Router) { }


  routerLinks=[
    {
      id:"dsb",
      routerText:"Dashboard",
      routerLink:"/admin/dashboard",
      routerIcon:"lni lni-dashboard"
    },
    {
      id:"inb",
      routerText:"Inbox",
      routerLink:"",
      routerIcon:"lni lni-inbox"
    },
    {
      id:"whl",
      routerText:"Withheld",
      routerLink:"",
      routerIcon:"lni lni-lock-alt"
    },
    {
      id:"apf",
      routerText:"Approved Form",
      routerLink:"",
      routerIcon:"lni lni-protection"
    },
    {
      id:"rtr",
      routerText:"Returned",
      routerLink:"",
      routerIcon:"lni lni-exit"
    },
    {
      id:"arc",
      routerText:"Archive",
      routerLink:"",
      routerIcon:"lni lni-archive"
    },
    {
      id:"mnp",
      routerText:"Manage Phase",
      routerLink:"",
      routerIcon:"lni lni-cog"
    },
    {
      id:"mgu",
      routerText:"Manage User",
      routerLink:"#",
      routerIcon:"lni lni-inbox",
      childs:[
        {
          routerText:"Agency",
          routerLink:"",
          routerIcon:"lni lni-apartment",
        },
        {
          routerText:"Agency",
          routerLink:"",
          routerIcon:"lni lni-apartment",
        },
      ]
    },
    {
      id:"mgc",
      routerText:"Manage Content",
      routerLink:"#",
      routerIcon:"lni lni-control-panel",
      childs:[
        {
          routerText:"About Us",
          routerLink:"/admin/manage-content",
          routerIcon:"lni lni-add-files",
        },
        {
          routerText:"Notification",
          routerLink:"/admin/manage-content",
          routerIcon:"lni lni-add-files",
        },
      ]
    },

  ]

  ngOnInit(): void {
    
  }

  isActive(url:string): boolean {
    return this.route.url.includes(url);
}

}
