import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageContentRoutingModule } from './manage-content-routing.module';
import { ManageContentComponent } from './manage-content.component';
import { QuillModule } from 'ngx-quill'

@NgModule({
  declarations: [
    ManageContentComponent
  ],
  imports: [
    CommonModule,
    ManageContentRoutingModule,
    QuillModule.forRoot()
  ]
})
export class ManageContentModule { }
