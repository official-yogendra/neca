import { Component, OnInit } from '@angular/core';
import { AdminHeaderService } from 'src/app/modules/core/services/admin/admin-header.service';

@Component({
  selector: 'app-manage-content',
  templateUrl: './manage-content.component.html',
  styleUrls: ['./manage-content.component.scss']
})
export class ManageContentComponent implements OnInit {

  quillConfig = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered' }, { 'list': 'bullet' }],
      [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
      [{ 'direction': 'rtl' }],                         // text direction

      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

    
      ['clean'],                                         // remove formatting button
      ['image']                         // ['link', 'image', 'video'] link and image, video
    ],
    //imageResize: true
  };

  constructor(private adminHeaderService:AdminHeaderService) { }

  ngOnInit(): void {
    this.adminHeaderService.headerText.next("Manage Content");
  }

}
