import { Component, OnInit } from '@angular/core';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { SampleList } from 'src/app/modules/core/modals/admin/dashboard.modal';
import { AdminHeaderService } from 'src/app/modules/core/services/admin/admin-header.service';
import { DashboardService } from 'src/app/modules/core/services/admin/dashboard.service';
import { AlertMessageService } from 'src/app/modules/shared/alert-message/alert-message.service';
import { LoaderService } from 'src/app/modules/shared/loader/loader.service';
import * as XLXS from 'xlsx';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  samplatList:SampleList;
  constructor(
    private dashboardService:DashboardService,
    private adminHeaderService:AdminHeaderService,
    private alertService:AlertMessageService) { }
  ngOnInit(): void {
    this.adminHeaderService.headerText.next("Dashboard");

    this.dashboardService.getAll().subscribe((response:SampleList)=>{
    this.samplatList=response;
    },
    err=>{
      this.alertService.error("Failed to load records !")
    });
  }

  pageChanged(event: PageChangedEvent): void {
    console.log(event.page);
  }

  onExportToExcelClick():void
  {

    //pass here the table id
    let element= document.getElementById('sample-list');
    const ws: XLXS.WorkSheet= XLXS.utils.table_to_sheet(element);

    //generate workbook and add the worksheet
    const wb:XLXS.WorkBook= XLXS.utils.book_new();
    XLXS.utils.book_append_sheet(wb,ws,"sheet1");

    //save the file
    XLXS.writeFile(wb,"sample-list-workbook.xlsx")

  }

}
