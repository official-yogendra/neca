import { Component, OnInit } from '@angular/core';
import { AdminHeaderService } from '../../core/services/admin/admin-header.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  pageHeaderText='';
  constructor(private adminLoaderService:AdminHeaderService) { }

  ngOnInit(): void {
    this.adminLoaderService.headerText.subscribe(headerText=>{
      this.pageHeaderText=headerText;
    });
  }

}
